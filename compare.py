#!/usr/bin/python3

import argparse
import os
import subprocess
import sys

def findSpackPackages(packagename, arg_exact=False):
	if arg_exact:
		searchString = packagename
	else:
		searchString = "*" + packagename + "*"

	out = subprocess.run(["find", "spack/var/spack/repos/builtin/packages", "-maxdepth", "1", "-iname", searchString], capture_output=True, text=True).stdout
	packageList = []
	lines = out.split('\n')[:-1]
	for line in lines:
		packageList.append(line.split('/')[6])

	packageList.sort()
	return packageList

def findEbPackages(packagename, arg_exact):
	if arg_exact:
		searchString = packagename + ".eb"
	else:
		searchString = "*" + packagename + "*.eb"

	out = subprocess.run(["find", "easybuild-easyconfigs/easybuild/easyconfigs", "-iname", searchString], capture_output=True, text=True).stdout
	packageList = []
	lines = out.split('\n')[:-1]
	for line in lines:
		if packagename.lower() in line.split('-')[1]:
			packageList.append(line.split('/')[-1])

	packageList.sort()
	return packageList

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("packages", metavar='Packages', type=str, nargs='*', help='One or more packages to look for, whitespace-separated.')
	parser.add_argument("-l", "--listfile", help="Specify a file containing a list of packages to search for. One item per line!")
	parser.add_argument("-e", "--exact", action="store_true", help="List only exact name matches, instead of matching for *<name>* (default).")
	parser.add_argument("-c", "--count", action="store_true", help="Instead of listing all matching packages, only list match count.")
	parser.add_argument("-u", "--unsorted", action="store_true", help="Whether not to sort the list of packages before searching.")
	parser.add_argument("-r", "--reclone", action="store_true", help="Re-clone the Spack and EasyBuild-easyconfigs repositories. Will DELETE existing 'spack' and 'easybuild-easyconfigs' directories!")

	args = parser.parse_args()

	packagelist = args.packages
	arg_exact = args.exact
	arg_count = args.count
	arg_unsorted = args.unsorted
	arg_reclone = args.reclone

	if not (len(sys.argv) > 1):
		print("No arguments specified, I assume you need help :-)\n")
		parser.print_help()
		exit(0)

	if args.listfile is not None:
		listfile = open(args.listfile, "r")
		for line in listfile:
			packagelist.append(line)

	if packagelist == []:
		print("You did not specify any packages to look for, so there is nothing to do. Exiting.")
		exit(0)

	if not arg_unsorted:
		packagelist.sort()

	if arg_reclone:
		print("Existing directories 'spack' and 'easybuild-easyconfigs' will be deleted! Ok? [y/n]")
		while True:
			user_input = sys.stdin.readline().strip().lower()
			if user_input == "y" or user_input == "yes":
				break
			if user_input == "n" or user_input == "no":
				exit(0)
			print("This was no valid input, try again!")

		subprocess.run("rm -rf spack easybuild-easyconfigs", shell=True)
		subprocess.run("git clone -c feature.manyFiles=true https://github.com/spack/spack.git", shell=True)
		subprocess.run("git clone https://github.com/easybuilders/easybuild-easyconfigs", shell=True)

	if not os.path.isdir("spack"):
		subprocess.run("git clone -c feature.manyFiles=true https://github.com/spack/spack.git", shell=True)
		print()

	if not os.path.isdir("easybuild-easyconfigs"):
		subprocess.run("git clone https://github.com/easybuilders/easybuild-easyconfigs", shell=True)
		print()

	for packagename in packagelist:
		packagename = packagename.strip()
		print("--- " + packagename + " " + (60 - len(packagename)) * "-")
		spackHits = findSpackPackages(packagename, arg_exact)
		ebHits = findEbPackages(packagename, arg_exact)
		spackHitCount = len(spackHits)
		ebHitCount = len(ebHits)

		if arg_count:
			print(spackHitCount + "\t" + ebHitCount)
		else:
			for i in range(max(spackHitCount, ebHitCount)):
				if i < spackHitCount:
					if i < ebHitCount:
						entryLength = len(spackHits[i])
						print(spackHits[i] + (50-entryLength) * " " + ebHits[i])
					else:
						print(spackHits[i])
				else:
					print(50 * " " + ebHits[i])
		print()

	exit(0)
